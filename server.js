'use strict';
var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var readability = require('readability2');
var htmlparser = require('htmlparser2');
var needle = require('needle');

var app = express();
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());

var server = app.listen(process.env.PORT || 8080, function () {
  var port = server.address().port;
  console.log("App now running on port", port);
});

// Generic error handler used by all endpoints.
function handleError(res, reason, message, code) {
  console.log("ERROR: " + reason + " MESSAGE: " + message);
  res.status(code || 500).json({"error": message});
}

// SEO-text parser
function createSeoText(readability, htmlparser, raw_html) {
  var reader = new (readability.Readability);
  var parser = new (htmlparser.Parser)({
    onopentag: function (name, attributes) {
      reader.onopentag(name);
      for (var a in attributes)
        reader.onattribute(a, attributes[a]);
    },
    onclosetag: reader.onclosetag.bind(reader),
    ontext: reader.ontext.bind(reader)
  }, {decodeEntities: true});

  parser.end(raw_html);

  var res  = reader.compute();
  var text = reader.clean(res);

  return res.heading + '\n' + text;
}

/*  "/text"
 *    GET: get SEO-text
 *    POST: send raw HTML
 */

app.post("/text", function(req, res) {

  var options = {
    follow_max: 5,
    follow_set_cookies: true,
    open_timeout: 20000,
    rejectUnauthorized: false
  }

  var url = req.body.url;

  if (!url) {
    handleError(res, "Invalid url", "Must provide a url.", 400);
  } else {
    needle.get(url, options, function(error, response) {
      if (!error && response.statusCode == 200) {
        res.status(200).json(
          { html: response.body,
            text: createSeoText(readability, htmlparser, response.body).trim() });
      } else if (error) {
        handleError(res, "Respond error", error.message + " URL: " + url, 500);
      } else {
        handleError(res, "Respond status", "Status: " + response.statusCode, 500);
      }
    });

  }
});
